﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*This is the Camera Controller class
 */
public class CameraController : MonoBehaviour {

    // Use this for initialization
    public const float SCROLLSPEED = 2f;
    public const float ZOOMSPEED = 8f;
    public const float MAXZOOM = 1f;
    public const float MINZOOM = 5f;
    Camera _camera;
    void Start () {

        _camera = gameObject.GetComponent<Camera>();

#if UNITY_EDITOR
        if (_camera == null)
        {
            Debug.LogError("Controller is not on an object with a camera component!");
        }
#endif

    }

    // Update is called once per frame
    void Update () {
  //      Debug.Log(GameState.cameraminx + "" + GameState.cameramaxx + "" + GameState.cameraminy + "" + GameState.cameramaxy);
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.up * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.down * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        
        if (Input.mousePosition.y >= Screen.height * 0.95)
        {
            transform.Translate(Vector3.up * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.mousePosition.y <= Screen.height - (Screen.height * 0.95))
        {
            transform.Translate(Vector3.down * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.mousePosition.x >= Screen.width * 0.95)
        {
            transform.Translate(Vector3.right * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        if (Input.mousePosition.x <= Screen.width - (Screen.width * 0.95))
        {
            transform.Translate(Vector3.left * Time.deltaTime * SCROLLSPEED, Space.World);
        }
        

        float difx = GameState.cameramaxx - GameState.cameraminx;
        float dify = GameState.cameramaxy - GameState.cameraminy;
        if (transform.position.x<  _camera.orthographicSize) {
            transform.Translate((_camera.orthographicSize) -transform.position.x,0,0);
        }
        
        if (transform.position.y >-_camera.orthographicSize + 0.32f)
        {
            transform.Translate(0, -(_camera.orthographicSize - 0.32f) - transform.position.y, 0);
        }
        
        if (transform.position.y <-(dify-_camera.orthographicSize+0.32f))
        {
            transform.Translate(0,-(dify-_camera.orthographicSize+0.32f) - transform.position.y, 0);
        }
        if (transform.position.x > difx-_camera.orthographicSize)
        {
            transform.Translate((difx-_camera.orthographicSize) - transform.position.x, 0, 0);
        }

        var scroll = Input.GetAxis("Mouse ScrollWheel");

        if (scroll < 0f)
        {
            _camera.orthographicSize = _camera.orthographicSize + ZOOMSPEED * 0.01f;
        }
        if (_camera.orthographicSize > Mathf.Max(Mathf.Abs(difx), Mathf.Abs(dify))/2+0.32f)
        {
            _camera.orthographicSize = Mathf.Max(Mathf.Abs(difx), Mathf.Abs(dify))/2+0.32f;
        }
        //     Debug.Log(_camera.orthographicSize);
        if (_camera.orthographicSize > 0.35)
        {
            if (scroll > 0f)
            {
                _camera.orthographicSize = _camera.orthographicSize - ZOOMSPEED*0.01f;
            }
        }
 //       Debug.Log(GameState.cameramaxx+" "+GameState.cameramaxy+" "+GameState.cameraminx+" "+GameState.cameraminy);
    }
}
