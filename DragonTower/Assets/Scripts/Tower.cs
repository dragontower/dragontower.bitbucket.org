﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Health))]
[RequireComponent (typeof (Attack))]
[RequireComponent (typeof (BoxCollider2D))]
public class Tower : MonoBehaviour {

	public GameObject barrel;
	Attack attack;
	Quaternion look;
	Vector3 direction;
	// Use this for initialization
	void Start () {
		gameObject.layer = (int)Layers.FRIENDLIES;	
		attack = gameObject.GetComponent<Attack>();
		barrel.transform.parent = transform;		
		var vec = barrel.transform.position;
		vec.z = -5;
		barrel.transform.position = vec;
		if(gameObject.activeSelf){
			barrel.SetActive(true);	
		}
	}
	
	// Update is called once per frame
	void Update () {
		var target = attack.Target();
		if(target != null){
			direction = (target.transform.position - barrel.transform.position).normalized;
			float rot_z = Mathf.Atan2(direction.y,direction.x) * Mathf.Rad2Deg;
			barrel.transform.rotation = Quaternion.Euler(0f,0f,rot_z + 90);
		}
	}
}
