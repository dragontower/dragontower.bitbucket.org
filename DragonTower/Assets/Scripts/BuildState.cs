﻿using UnityEngine;
using System.Collections;

public class BuildState : MonoBehaviour {

	public bool wall;
	public bool cannon;
	public bool gun;
	public GameObject frame;
	// Use this for initialization
	bool disbale;
	Rect max,min;
	void Start () {
		max = frame.GetComponent<UnityEngine.UI.Image>().canvas.pixelRect;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(max);
		if(GameState.currentGameStateEnum == GameStateEnum.BUILDING){
				switch(GameState.currentBuildState){
					case BuildStateEnum.DELETING:
						if(Input.mousePosition.x >340 && Input.mousePosition.x < 610 && Input.mousePosition.y < 95){}else
						if(Input.mousePosition.x >860 && Input.mousePosition.x < 955 && Input.mousePosition.y > 865){}else
						if(Input.GetMouseButtonDown(0) && !disbale){
							Debug.Log(Input.mousePosition);
							var hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y),Vector2.zero,0f);
							if(hit){
								if(hit.collider.gameObject.layer == (int)Layers.FRIENDLIES){
										Debug.Log("test");
										var cost =	hit.collider.gameObject.GetComponent<Health>().goldValue/2;
										GameState.gold += cost;
										Destroy(hit.collider.gameObject);
								}
							}
						}
						break;
					case BuildStateEnum.BUILDING:
						if(Input.mousePosition.x >340 && Input.mousePosition.x < 610 && Input.mousePosition.y < 95){}else
						if(Input.mousePosition.x >860 && Input.mousePosition.x < 955 && Input.mousePosition.y > 865){}else
						if(Input.GetMouseButtonDown(0)){
							Debug.Log(Input.mousePosition);
							var hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y),Vector2.zero,0f);
							if(hit){
								if(hit.collider.gameObject.layer == (int)Layers.GROUND){
									var cost = Samples.buildings[(int)GameState.currentSelection].GetComponent<Health>().goldValue;
									if(GameState.gold > cost){
										GameState.gold -= cost;
										var building = Instantiate(Samples.buildings[(int)GameState.currentSelection]);
										building.SetActive(true);
										building.transform.position = hit.collider.transform.position;
										building.transform.parent = transform;
									}
								}
							}
						}
						break;
			}
		}
	}
	/* This function switches to the build state.
	 * It has no parameters.
	 */
	public void ToggleCannon(){
		GameState.currentSelection = 1;
	}
	public void ToggleGun(){
		GameState.currentSelection = 0;
	}
	public void ToggleWall(){
		GameState.currentSelection = 2;
	}
	public void ToggleBuild(){
		GameState.currentBuildState = BuildStateEnum.BUILDING;	
	}
	public void ToggleDelete(){
		GameState.currentBuildState = BuildStateEnum.DELETING;	
	}
	public void DisbaleBuild(){
		disbale = true;
	}
}
