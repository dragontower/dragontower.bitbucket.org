﻿//TODOS
//TODO: Add spawing code for the attacking phase.
//TODO: Add building code for the building phase.
//TODO: sort out all of the parent child relationships for the game.
//TODO: Possibly make a map script to manage the building of towers.
//TODO: Implement the money system.
//TODO: Win/lose conditions.
//TODO: Display gold, score, wave number, and waves left.
//TODO: Have the game state track all of the different units.
//TESTING
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public enum BuildStateEnum{
	BUILDING,
	UPGRADING,
	DELETING,
}
/*This Enum describes the current state of the game.
 *BUILDING: is the build phase of the game, when the player can place towers, and walls.
 *ATTACKING: is the attacking phase when the spawner become active and release there monsters.
 */
public enum GameStateEnum{
	STARTING_BUILDING,
	BUILDING,
	STARTING_ATTACKING,
	ATTACKING,
	GAME_OVER,
}
/*This is the static class which provide global information about the games state to the rest of the game.
 */
public static class GameState{
	//Shuns Code---------
	public static float map;
    public static float cameramaxx;
    public static float cameraminx;
    public static float cameramaxy;
    public static float cameraminy;
	//-----------------
	public static GameStateEnum currentGameStateEnum; //This is the current state of the game.
	public static BuildStateEnum currentBuildState;
	public static uint wave; //This is the number of waves left in the attack phase.
	public static uint numberOfWaveLeft;
	public static uint numberOfActiveSpawners;
	public static uint numberOfSpawners;
	public static uint numberOfMonstersAlive;
	public static bool showRange;
	public static bool showHealth;
	public static uint currentSelection;
	public static bool mapNeedsReset;
	public static Transform goal; //This is the location which the monsters will attempt to path to.
	public static uint gold; //This is the amount of gold a player has.
	public static uint score; //This is the players score (right now a sum of all gold earned).
	//This function keeps track of all the different things that are destroyed.
	public static void Destroy(int layer,uint goldValue){
		switch (layer){
			case (int)Layers.ENEMIES:
				gold += goldValue;
				score += goldValue;
				numberOfMonstersAlive--;
				break;
			case (int)Layers.FRIENDLIES:
				mapNeedsReset = true;				
				break;
		}
	}
}
/*This is the component which will control the game state, make sure to place it as a parent of all of the game objects.
 *	Notes: 
 *	-Only one per level scene.
 *	-This should be placed in and empty game object which has all other map object as its children.
 */
[RequireComponent(typeof (BuildState))]
public class GameStateController : MonoBehaviour {
	//Varibles for the phase change texts 
	const float TIME_TO_DISPLAY_TEXT = 1.0f; //The length of time in seconds the phase change text will appear 
	//Varibles for the attack phase message;
	bool showAttackingText;
	float textTimerStart,textTimerCount;
	GUIStyle attackMessageStyle;
	//Varibles for the build phase message;
	bool showBuildingText;
	GUIStyle buildMessageStyle;
	public GameObject canvas;
	public GameObject ui;
	public GameObject gameOver;
	public GameObject attackPhase;
	public GameObject buildPhase;
	public GameObject waves;
	public GameObject gold;
	public GameObject monster;
	public string nextlevel;
	//Set by the game designer
	public uint numberOfWaves; //This is the number of waves for this level.
	public uint startingGold = 1000;
	public Transform goal; //This is the goal for the monsters to move towards.
	// Use this for initialization
	void Start () {
		GameState.showHealth = true;
		//Set up the game state;
		GameState.goal = goal;
		GameState.numberOfWaveLeft = numberOfWaves;
		GameState.gold = startingGold;
		//Set up the attack message style
		attackMessageStyle = new GUIStyle();
		attackMessageStyle.fontSize = 50;
		attackMessageStyle.normal.textColor = Color.red;
		//Set up the build message style
		buildMessageStyle = new GUIStyle();
		buildMessageStyle.fontSize = 50;
		buildMessageStyle.normal.textColor = Color.red;
		SwitchToBuildState();
		//Shuns code ------------------
		//get the mapsize for the controller
        var list = new List<Transform>();
        var objects = gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform transform in objects)
        {
            if (transform.gameObject.layer == 8)
            {
                list.Add(transform);
            }
        }
        GameState.cameraminx = list[0].position.x;
        GameState.cameramaxx = list[0].position.x;
        GameState.cameraminy = list[0].position.y;
        GameState.cameramaxy = list[0].position.y;
        foreach (Transform transform in list) {
            if (transform.position.x> GameState.cameramaxx) {
                GameState.cameramaxx = transform.position.x;
            }
            if (transform.position.y > GameState.cameramaxy)
            {
                GameState.cameramaxy = transform.position.y;
            }
            if (transform.position.x < GameState.cameraminx)
            {
                GameState.cameraminx = transform.position.x;
            }
            if (transform.position.y < GameState.cameraminy)
            {
                GameState.cameraminy = transform.position.y;
            }
//            Debug.Log(transform.position.x+" " + transform.position.y);
        }
	}
	// Update is called once per frame
	void Update () {
		waves.GetComponent<UnityEngine.UI.Text>().text = GameState.numberOfWaveLeft.ToString();
		gold.GetComponent<UnityEngine.UI.Text>().text = GameState.gold.ToString();
		monster.GetComponent<UnityEngine.UI.Text>().text = GameState.numberOfMonstersAlive.ToString();
		if(Input.GetKeyDown(KeyCode.U)){
			ui.SetActive(!ui.activeSelf);
		}	
		if(Input.GetKeyDown(KeyCode.Escape)){
			canvas.SetActive(!canvas.activeSelf);
			if(Time.timeScale == 1){
				Time.timeScale = 0;
			}else{
				Time.timeScale = 1;	
			}
		}
		if(GameState.mapNeedsReset){
			AstarPath.active.Scan();
			GameState.mapNeedsReset = false;
		}
		if(GameState.goal == null){
			GameState.currentGameStateEnum = GameStateEnum.GAME_OVER;	
		}
		if(Input.GetKeyDown(KeyCode.LeftAlt)){
			GameState.showRange = !GameState.showRange;	
		}
		if(Input.GetKeyDown(KeyCode.H)){
			GameState.showHealth = !GameState.showHealth;	
		}
		//Determine the game state 
		//TODO: Make it faster so theres no switch statement every update function.
		switch(GameState.currentGameStateEnum){
			case GameStateEnum.STARTING_ATTACKING:
				GameState.numberOfWaveLeft--;
				GameState.currentGameStateEnum = GameStateEnum.ATTACKING;
				break;
			case GameStateEnum.ATTACKING:
				if(GameState.numberOfActiveSpawners <= 0 && GameState.numberOfMonstersAlive <= 0){
					SwitchToBuildState();
					break;
				}
				break;
			case GameStateEnum.STARTING_BUILDING:
				GameState.currentGameStateEnum = GameStateEnum.BUILDING;
				break;
				//Check if the player has pressed space, which will start the attack phase.
			case GameStateEnum.BUILDING:
				if(GameState.numberOfWaveLeft <1){
					Debug.Log("switch");
					Application.LoadLevel(nextlevel);	
				}
				if(Input.GetKeyDown(KeyCode.Space)){
					SwitchToAttackState();
					break;
				}
				break;
			case GameStateEnum.GAME_OVER:
				gameOver.SetActive(true);
				if(Input.GetKeyDown(KeyCode.Space)){
					Application.LoadLevel(Application.loadedLevel);	
					break;
				}
				break;
		}	
		//This is code related to the timer to display the text.
		if(showAttackingText){
			textTimerCount = Time.time;
			attackPhase.SetActive(true);
		}
		if(showBuildingText){
			textTimerCount = Time.time;	
			buildPhase.SetActive(true);
		}
		if(textTimerCount - textTimerStart > TIME_TO_DISPLAY_TEXT && showAttackingText){
			attackPhase.SetActive(false);
			showAttackingText = false;		
		}
		if(textTimerCount - textTimerStart > TIME_TO_DISPLAY_TEXT && showBuildingText){
			buildPhase.SetActive(false);
			showBuildingText = false;		
		}
		
	}
	/* This function switches to the attack state.
	 * It has no parameters.
	 */
	void SwitchToAttackState(){
		foreach(Transform transform in gameObject.GetComponentsInChildren<Transform>()){
			if(transform.gameObject.layer == (int) Layers.GROUND){
				Destroy(transform.gameObject.GetComponent<BoxCollider2D>());
			}
		}
		AstarPath.active.Scan();
		GameState.wave++;
		GameState.currentGameStateEnum = GameStateEnum.STARTING_ATTACKING;	
		GameState.numberOfActiveSpawners = GameState.numberOfSpawners;
		//Start the attack phase text change.
		showAttackingText = true;
		textTimerStart = Time.time;
	}
	void SwitchToBuildState(){
		GameState.currentBuildState = BuildStateEnum.BUILDING;
		foreach(Transform transform in gameObject.GetComponentsInChildren<Transform>()){
			if(transform.gameObject.layer == (int) Layers.GROUND){
				transform.gameObject.AddComponent<BoxCollider2D>();
			}
		}
		GameState.currentGameStateEnum = GameStateEnum.STARTING_BUILDING;	
		//Start the build phase text change.
		showBuildingText = true;
		textTimerStart = Time.time;
	}
	/* The gui render func
	 * Just controls the gui rendering, make sure to place all GUI related call in this function.
	 */
	void OnGUI(){
		
	}
	public void ChangeToScene (string sceneToChangeTo) {
        Application.LoadLevel(sceneToChangeTo);
	}
	public void Quit(){
		Application.Quit();	
	}
	public void Resume(){
		canvas.SetActive(false);
		Time.timeScale = 1.0f;	
	}
	public void Restart(){
		Application.LoadLevel(Application.loadedLevel);	
		Time.timeScale = 1.0f;
	}
}
