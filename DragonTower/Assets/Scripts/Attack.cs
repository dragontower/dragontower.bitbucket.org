﻿//TODOS
//TODO:Add different types of attacks.
//TODO:Add different types of targeting.
//TESTING
//TODO:Targeting.
//TODO:Focus Attacking.
//TODO:Multitasking.
//TODO:Correct cycling.
//TODO:Dealing with dead units.
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*This is an enum describing the different types of damage that the dealt
 * INSTANT: Deal the damage as soon as the attack happens
 */
public enum DamageType{
	INSTANT,
}
public enum AttackState{
	LOOKING_FOR_TARGETS,
	SHOOTING,
	COOLDOWN,
	COOLDOWN_ENDED,
	STARTING_COOLDOWN,

}
/*This the attack component for both monsters and towers
 * Notes:
 * -Make sure to provide a gameObject which has a projectile script
 * -Make sure to provide tags for it to target.
 */
public class Attack : MonoBehaviour {
	public float cooldownInSeconds; //This the amount of time it takes for the attack to attack again.
	public int targetingRangeInPixels; //This is targeting distance in pixels.
	public Layers layerToAttack; //This is tag which you want the attack to hit.
	public GameObject bulletSample; //This is the bullet which will be shot.
	GameObject target;
	float cycleTimerCount; //This tracks the current time during the attack cycle.
	Layers defualtLayers;
	float cycleTimerStart; //This tracks when the attack cycle has started.
	AttackState currentAttackState;
	//Gets assigned be the sample manager
	GameObject rangeCircleClone;
	// Use this for initialization
	void Start () {
		defualtLayers = layerToAttack;
		//Set up the range circle
		if(Samples.range == null){
			rangeCircleClone = Instantiate(transform.parent.GetComponent<SampleManager>().range);
		}else{
			rangeCircleClone = transform.FindChild("Range(Clone)").gameObject;
			rangeCircleClone.SetActive(true);
		}
		rangeCircleClone.transform.position = transform.position;
		rangeCircleClone.transform.parent = gameObject.transform;
		rangeCircleClone.transform.Translate(0,0,-2);
		rangeCircleClone.GetComponent<Circle>().radius = (float)targetingRangeInPixels/100;
		rangeCircleClone.GetComponent<Circle>().Rebuild();
		rangeCircleClone.GetComponent<MeshRenderer>().enabled = GameState.showRange;
		currentAttackState = AttackState.LOOKING_FOR_TARGETS;
	}
	
	//NOTE: I commented this heavily to make sure the logic for the different attacks works correctly.
	// Update is called once per frame
	void Update () {
		rangeCircleClone.GetComponent<MeshRenderer>().enabled = GameState.showRange;
		switch(GameState.currentGameStateEnum){
			case GameStateEnum.ATTACKING:
				switch(currentAttackState){
					case AttackState.LOOKING_FOR_TARGETS:
						if(AquireTarget()){
							Debug.Log(target.name);
							currentAttackState = AttackState.SHOOTING;
							break;
						}
						break;
					case AttackState.SHOOTING:
						if(target == null){
							currentAttackState = AttackState.LOOKING_FOR_TARGETS;	
						}
						Shoot();
						currentAttackState = AttackState.STARTING_COOLDOWN;
						break;
					case AttackState.STARTING_COOLDOWN:
						cycleTimerStart = Time.time;
						currentAttackState = AttackState.COOLDOWN;
						break;
					case AttackState.COOLDOWN:
						cycleTimerCount = Time.time;
						if(cycleTimerCount-cycleTimerStart > cooldownInSeconds){
							currentAttackState = AttackState.COOLDOWN_ENDED;	
							break;
						}
						break;
					case AttackState.COOLDOWN_ENDED:
						if(target != null && Vector3.Distance(target.transform.position,transform.position) < (float)targetingRangeInPixels/100){
							currentAttackState = AttackState.SHOOTING;
							break;
						}else{
							currentAttackState = AttackState.LOOKING_FOR_TARGETS;		
							break;
						}
				}
				break;
		}
	}
	public void ResetAttackLayer(){
		layerToAttack = defualtLayers;
	}
	//This launches the projectile from the unit.
	void Shoot(){
		if(target != null){
			var bullet = Instantiate(bulletSample);
			bullet.SetActive(true);
			bullet.transform.position = transform.position;
			bullet.transform.parent = transform;
			bullet.GetComponent<Projectile>().target = target;
		}
	}
	//This function find targets within the range and parameters of the unit's attack.
	bool AquireTarget(){
		var hitcolliders = Physics2D.OverlapCircleAll(transform.position,((float)targetingRangeInPixels)/100);	
		foreach (Collider2D collider in hitcolliders){
			//make sure the unit is active.
			if(collider.gameObject.layer == (int)layerToAttack){
				target = collider.transform.gameObject;
				return true;
			}
		}
		return false;
	}
	//This returns true if currently attacking.
	public AttackState GetAttackState(){
		return currentAttackState;	
	}
	public GameObject Target(){
		return target;	
	}
}
