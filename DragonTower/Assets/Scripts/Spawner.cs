﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public enum SpawnerType{
		LINEAR,
		EXPONENTIAL,
	}
	enum SpawnCycle{
		START_OF_SPAWNING,
		SPAWNING,
		COOLDOWN,
		NOT_SPAWNING,
	}
	//Set by the game designer
	public SpawnerType spawnerType;
	public uint rateOfMonsterSpawnIncrease;
	public uint baseMonsterSpawn;
	public float rateOfTimeOffSetDecrease;
	public float baseOffsetInSeconds;
	float spawnCycleStart;
	float spawnCycleCount;
	public GameObject monster;
	SpawnCycle currentSpawnCycle;
	uint currentNumberOfMonsterToSpawn;
	uint currentWave;
	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(2,2,1);
		gameObject.GetComponent<SpriteRenderer>().sprite = monster.GetComponent<SpriteRenderer>().sprite;
		GameState.numberOfSpawners++;
		gameObject.layer = (int)Layers.SPAWNER;
	}
	// Update is called once per frame
	void Update () {
		switch (GameState.currentGameStateEnum){
			case GameStateEnum.STARTING_ATTACKING:
				currentSpawnCycle = SpawnCycle.START_OF_SPAWNING;
				return;
			case GameStateEnum.ATTACKING:
				switch(currentSpawnCycle){
					case SpawnCycle.START_OF_SPAWNING:
						currentNumberOfMonsterToSpawn = baseMonsterSpawn + (rateOfMonsterSpawnIncrease * (GameState.wave-1));
						currentSpawnCycle = SpawnCycle.SPAWNING;
						return;
					case SpawnCycle.SPAWNING:
						if(currentNumberOfMonsterToSpawn > 0){
							spawnCycleStart = Time.time;
							currentNumberOfMonsterToSpawn--;	
							GameState.numberOfMonstersAlive++;
							var newMonster = Instantiate(monster);
							newMonster.SetActive(true);
							newMonster.transform.position = transform.position;
							newMonster.transform.parent = transform;
							currentSpawnCycle = SpawnCycle.COOLDOWN;
							return;
						}
						GameState.numberOfActiveSpawners--;
						currentSpawnCycle = SpawnCycle.NOT_SPAWNING;
						return;
					case SpawnCycle.COOLDOWN:
						spawnCycleCount = Time.time;
						if(spawnCycleCount - spawnCycleStart > Mathf.Clamp(baseOffsetInSeconds - rateOfTimeOffSetDecrease*(GameState.wave-1),0f,baseOffsetInSeconds)){
							currentSpawnCycle = SpawnCycle.SPAWNING;
							return;
						}
						return;
				}
				return;
		}
	}
}
