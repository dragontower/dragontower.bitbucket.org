﻿//TODOS
//TODO: Have move the movement code from the fixedupdate to the update function.
//TODO: Move the spawning script to the unit script.
//TODO: Move the target assignment to the start up script if possible.
using UnityEngine;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
public enum MovementAiType{
	GOAL,
	AGGRESIVE,
}
/*This component is for any unit that is a mosnter, it handles all of the mosnters, movement and attack conditions.
 * Notes:
 * -Requires health,unit, and attack components.
 * -It sets the tag to Monster
 */
[RequireComponent (typeof (Health))]
[RequireComponent (typeof (Attack))]
[RequireComponent (typeof (Seeker))]
[RequireComponent (typeof (BoxCollider2D))]
public class Monster : MonoBehaviour {
	public uint toughness; //This is how hard the monster is (assigned by the design based on their own discrestion).
	public float speed = 2; //This is how fast the unit moves, the number is based on 0.02 pixel units per fixed update calls (confirm this???).
	public Layers aggressiveLayerTarget;
	public Sprite up;
	public Sprite down;
	public Sprite left;
	public Sprite right;
	public uint sight = 100000;
	bool resetOfTarget = false;
	GameObject targetObj;
	Health health;
	Attack attack;
	Vector3 origPos;
	SpriteRenderer spriteRenderer;
	uint stepper;
	// Use this for initialization
	void Start () {	
		origPos = transform.position;
		health = gameObject.GetComponent<Health>();
		attack = gameObject.GetComponent<Attack>();
		seeker = gameObject.GetComponent<Seeker>();
		gameObject.layer = (int)Layers.ENEMIES;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		//Set the right tag.
		//Tempoary will be called via the spawner.
		Spawn();		
	}
	public void Spawn(){
		AquireTarget();
	}	
	bool AquireTarget(){
		var hitcolliders = Physics2D.OverlapCircleAll(transform.position,((float)sight)/100);	
		foreach (Collider2D collider in hitcolliders){
			//make sure the unit is active.
			if(collider.gameObject.layer == (int)aggressiveLayerTarget){
				targetObj = collider.transform.gameObject;
				target = targetObj.transform;
				BeginPathing();
				return true;
			}
		}
		return false;
	}
	// Update is called once per frame
	void Update () {
	 if(target == null){
		AquireTarget();
	 }
     Vector3 moveDirection = gameObject.transform.position - origPos; 
     if (moveDirection != Vector3.zero) 
     {
         float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
		 if(angle < 190 && angle > 170){
			spriteRenderer.sprite = left;	 
		 }else
		 if(angle < 10 && angle > -10){
			spriteRenderer.sprite = right; 
		 }else
		 if(angle < 100 && angle > 80){
			spriteRenderer.sprite = up; 
		 }else
		 if(angle < -80 && angle > -100){
			spriteRenderer.sprite = down; 
		 }
     }
		origPos = transform.position;	 
	 
	}
	//Movement AI
	//The point to move to
  	Transform target;
  	private Seeker seeker;
  	//The calculated path
  	public Path path;

  	//The max distance from the AI to a waypoint for it to continue to the next waypoint
  	float nextWaypointDistance = 0.2f;

  	//The waypoint we are currently moving towards
  	private int currentWaypoint = 0;

  	public void BeginPathing()
  	{
  	  //Start a new path to the targetPosition, return the result to the OnPathComplete function
  	  seeker.StartPath( transform.position, target.position, OnPathComplete );
  	}

  	public void OnPathComplete ( Path p )
  	{
  	  if (!p.error)
  	  {
  	    path = p;
  	    //Reset the waypoint counter
  	    currentWaypoint = 0;
  	  }else{
	  }
  	}

  	public void FixedUpdate ()
  	{
	  attack.ResetAttackLayer();

  	  if (path == null)
  	  {
  	    return;
  	  }

  	  if (currentWaypoint >= path.vectorPath.Count)
  	  {
		attack.layerToAttack = Layers.FRIENDLIES;
		if(target != null){
			BeginPathing();
		}
  	    return;
  	  }else{
	  }

  	  //Direction to the next waypoint
  	  Vector3 dir = ( path.vectorPath[ currentWaypoint ] - transform.position ).normalized;
  	  dir *= speed * Time.fixedDeltaTime;
  	  this.gameObject.transform.Translate( dir );

  	  //Check if we are close enough to the next waypoint
  	  //If we are, proceed to follow the next waypoint
  	  if (Vector3.Distance( transform.position, path.vectorPath[ currentWaypoint ] ) < nextWaypointDistance)
  	  {
  	    currentWaypoint++;
  	    return;
  	  }
  	}
}
