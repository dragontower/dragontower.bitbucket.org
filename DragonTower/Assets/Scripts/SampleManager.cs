﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Layers{
	GROUND = 8,
	FRIENDLIES = 9,
	PROJECTILES = 10,
	ENEMIES = 11,
	SPAWNER = 12,
	GOAL = 13,
}
public static class Samples{
	public static GameObject range;
	public static List<GameObject> buildings;
	public static bool loaded;
	public static Texture deleteTexture;
}
//Manages all of the game object which ares as sample for others
public class SampleManager : MonoBehaviour {
	
	public GameObject range;
	public Texture delete;
	// Use this for initialization
	void Start () {
		Samples.deleteTexture = delete;
		Samples.range = GameObject.Find("Range");
		Samples.buildings = new List<GameObject>();
		foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>()){
			transform.gameObject.SetActive(false);
			if(transform.gameObject.layer == (int)Layers.FRIENDLIES){
				Samples.buildings.Add(transform.gameObject);
			}
		}	
		Samples.loaded = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
