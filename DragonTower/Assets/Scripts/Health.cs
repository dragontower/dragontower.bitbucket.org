//TODOS
//TODO: Add the GUI for the health (healthbars).
//TODO: Make the health GUI relative to the units health;
//TESTING:
//TODO: Test recieving damage.
//TODO: Test destroying the game object.
//
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
/*This component manages the health of an object
 * Notes:
 * -Only one per gameObject.
 * -Use for both enemies and friendlies
 * -For now health is measured as an for simplicty sake, later may change to double for math sake. 
 * -This game object requires no other components to work
 */
public class Health : MonoBehaviour {
	public float health = 100; //This is the total starting health of a unit.
	public uint goldValue = 100;
	bool realInit = false;
	Rect spriteRect;
	float currentHealth; //This is the current health of the unit.
	GameObject healthScaleParent;
	float precentage;
	GameObject healthBar,damageBar;
	// Use this for initialization
	void Start () {
		currentHealth = health;
		precentage =1;
	}
	
	/*This function is called when a unit takes damage, for now don't call via message as it seems that the reflection is slower. 
	 */
	public void TakeDamage(uint damage){
		currentHealth = currentHealth - damage;	
		precentage = currentHealth/health;
	}
    // Update is called once per frame
    void Update () {
		if(realInit == false){
			realInit = true;	
			//Sprite for sizing
			spriteRect = gameObject.GetComponent<SpriteRenderer>().sprite.rect;

			//Health Parent for the health bar scaling
			healthScaleParent = new GameObject();
			healthScaleParent.transform.parent = transform;
			//Health bar
			healthBar = Instantiate(GameObject.Find("HealthBar"));
			healthBar.transform.parent = healthScaleParent.transform;
			healthBar.transform.localScale = new Vector3(spriteRect.width,1,1);

			//Health parent vec
			var vecP = new Vector3(-spriteRect.width/200f,spriteRect.height/200f + 0.1f, -7f);
			healthScaleParent.transform.localPosition = vecP;
			//Vec for the health bar
			var vecH = new Vector3(spriteRect.width/200,0,0); 
			healthBar.transform.localPosition = vecH;

			//Damage Bar
			damageBar = Instantiate(GameObject.Find("DamageBar"));
			damageBar.transform.parent = transform;
			damageBar.transform.localScale = new Vector3(spriteRect.width,1,1);
			//Vec for the damage bar
			var vecD = new Vector3(0,spriteRect.height/200f+0.1f,-6f);
			damageBar.transform.localPosition = vecD;
		}
		healthScaleParent.transform.localScale = new Vector3(precentage,1,1);
		if(currentHealth > health){
			Kill();	
		}

        if (currentHealth <= 0){
			Kill();
        }

		if(GameState.showHealth == false){
			healthBar.SetActive(false);	
			damageBar.SetActive(false);
		}
		if(GameState.showHealth == true && healthBar.activeSelf == false){
			healthBar.SetActive(true);	
			damageBar.SetActive(true);
		}
	}
	public void Kill(){
		//Register the death with the game state
		GameState.Destroy(gameObject.layer,goldValue);
		//Destroy itself.
    	Destroy(gameObject);
	}
	public float CurrentHealth(){
		return currentHealth;
	}
}
