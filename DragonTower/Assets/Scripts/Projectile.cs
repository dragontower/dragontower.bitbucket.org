﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public GameObject target;
	public int speedPixelsPerSecond;
	public int hitRadiusInPixels;
	public uint damage;

	// Use this for initialization
	void Start () {
		gameObject.layer = (int)Layers.PROJECTILES;	
	}
	
	// Update is called once per frame
	void Update () {
		if(target == null){
			Destroy(gameObject);
		}else{
			var distanceVec = target.transform.position - transform.position;
			if(distanceVec.magnitude < (float)hitRadiusInPixels/100){
				target.GetComponent<Health>().TakeDamage(damage);
				Destroy(gameObject);
			}
			var translateVec = (distanceVec).normalized*((float)speedPixelsPerSecond/100);
			transform.Translate(translateVec);
		}
	}
}
