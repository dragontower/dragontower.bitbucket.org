﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Health))]
[RequireComponent (typeof (BoxCollider2D))]
public class Wall : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.layer = (int)Layers.FRIENDLIES;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
